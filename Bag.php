<?php

namespace Rakit\Bag;

use ArrayAccess;
use Rakit\Util\Arr;

class Bag implements ArrayAccess {

    protected $items = array();
    protected $namespaces = array();
     
    public function __construct(array $initial_values = array()) {
        $this->items = $initial_values;
    }
     
    public function has($key) {
        return Arr::has($this->items, $key);
    }

    public function set($key, $value) {
        if(is_array($key)) {
            $this->items = $key;
            return;
        }
        
        Arr::set($this->items, $key, $value);
    }

    public function get($key, $default = null) {
        return Arr::get($this->items, $key, $default);
    }
     
    public function remove($key) {
        Arr::remove($this->items, $key);
    }

    public function all($deep = true) {
        $items = $this->items;

        if($deep) {
            foreach($this->namespaces as $ns => $bag) {
                $items[$ns] = $bag->all(true);
            }
        }

        return $items;
    }

    public function count($deep = false)
    {
        $items = $this->all($deep);
        return count($items);
    }

    public function size($deep = true)
    {
        $values = Arr::flatten($this->all($deep));
        return count($values);
    }

    public function offsetSet($key, $value) {
        $this->set($key, $value);
    }

    public function offsetExists($key) {
        return $this->has($key);
    }

    public function offsetUnset($key) {
        $this->remove($key);
    }

    public function offsetGet($key) {
        return $this->get($key, null);
    }
     
    public function __set($namespace, $value) {
        $bag = new Bag();
        $this->namespaces[$namespace] = $bag;
       
        if(is_array($value)) {
            $bag->set($value, null);
        }
    }

    public function __get($namespace) {
        if(isset($this->namespaces[$namespace])) {
            return $this->namespaces[$namespace];
        } else {
            throw new \Exception("Undefined config namespace {$namespace}");
        }
    }

}
