Rakit Bag
==================

PHP Bag library using ArrayAccess that support dot notation for setter and getter

## Installation

At this time, i have not submit this package into [packagist](http://packagist.org/). 
So you can install it via [composer](https://getcomposer.org) by add this repository in your composer.json and require it.

Your `composer.json` should look like this:

```json
  "require": {
    "rakit/bag": "dev-master"
  },
  "repositories": [
    {
      "type": "git",
      "url": "https://emsifa@bitbag.org/emsifa/rakit-bag.git"
    }
  ]
```

## Usage Examples

#### Initialize

```php
<?php

use Rakit\Bag\Bag;

$config = new Bag(array(
  'site_name' => 'My Store',
  'debug' => true
));

// there is two way to get the configuration

// using array access
echo $config["site_name"];        // 'My Store'

// or using get method
echo $config->get("site_name");   // 'My Store'
```

#### Get configuration

```php
<?php

$config = new Bag(array(
  'foo' => array(
    'bar' => 'foobar',
    'baz' => 'foobaz'
  )
));

// using dot notation
echo $config["foo.bar"]; // foobar

// using get()
echo $config->get("foo.bar"); // foobar

// how about getting undefined key? 
var_dump( $config->get("this.key.is.undefined") ); // NULL
var_dump( $config["this.key.is.undefined"] ); // NULL

// using get() allow you to define default value
var_dump( $config->get("this.key.is.undefined", "i am default value") ); // "i am default value"

```

#### Set configuration

```php
<?php

$config = new Bag;

// using set($key, $value)
$config->set("foo", "foo");
echo $config["foo"]; // foo

// using array access
$config["foo"] = "foooo";
echo $config["foo"]; // foooo

```

#### Working with dot notation

Dot notation string allow you to create array configuration without pain about undefined key. 
Ah let me show you the different...

Using native array as configuration

```php
<?php

$config = array();

$config["foo"]["bar"] = "foobar"; // << this code will give you error undefined key "foo"
echo $config["foo"]["bar"]; // << and also this code
```

Using `rakit/bag` with dot notation

```php
<?php

$config = new Bag;

// get data with dot notation
echo $config["foo.bar"]; // will return NULL, because there is no $config["foo"]["bar"]

// set data with dot notation
$config["foo.bar"] = "foobar";
$config["foo.baz"] = "foobaz";

/* now the configuration will look like this
    array(
      'foo' => array(
        'bar' => 'foobar',
        'baz' => 'foobaz'
      )
    )
 */

// so...
echo $config["foo.bar"]; // foobar
echo $config["foo.baz"]; // foobaz

print_r($config["foo"]); // array('bar' => 'foobar', 'baz' => 'foobaz')

```

#### Namespacing

```php
<?php

$config = new Bag;

// add blog namespace, just set new property into config object
$config->blog = array(
  'title' => 'My Blog',
  'db' => array(
    'host' => 'localhost',
    'username' => 'db_blog_user',
    'password' => 'db_blog_password',
    'dbname' => 'db_blog'
  )
);

// add store namespace
$config->store = array(
  'title' => 'My Store',
  'db' => array(
    'host' => 'localhost',
    'username' => 'db_store_user',
    'password' => 'db_store_password',
    'dbname' => 'db_store'
  )
);

// get data from namespace
echo $config->store->get("db.dbname"); // db_store
echo $config->blog->get("db.dbname"); // db_blog

// get using array access with dot notation
echo $config->store["db.dbname"]; // db_store
echo $config->blog["db.dbname"]; //db_blog

// set/change data
$config->store["db.dbname"] = 'my_store_db';
$config->store->set("db.username", "my_store_user");
$config->store->set("db.driver", "mysqli");

print_r($config->store["db"]);
/** 
 *  array(
 *    'host' => 'localhost', 
 *    'username' => 'my_store_user', 
 *    'password' => 'db_store_password',
 *    'dbname' => 'my_store_db',
 *    'driver' => 'mysqli'
 *  )
 */

```
